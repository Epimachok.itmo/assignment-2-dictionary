%include "lib.inc"

section .text
global find_word
extern string_equals

find_word:
	xor rax, rax
	.loop:
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		cmp rax, 1
		jz .ok
		mov rsi, [rsi] ;
		test rsi, rsi
		jz .error
		jmp .loop
	.ok:
		mov rax, rsi
		ret
	.error:
		xor rax, rax
		ret
