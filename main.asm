%include "lib.inc"
%include "words.inc"
%define buffer_size 256

section .rodata
	hello: db "Enter the key: ", 0
	ok: db "Find element with this key: " 
	overflow: db "Input string is too long", 0
	error: db "Incorrect key", 0
	
section .bss
buffer: resb buffer_size


section .text

global _start

extern find_word

_start:
	mov rdi, hello
	call print_string
        mov rdi, buffer
        mov rsi, buffer_size
	call read_word
       	test rax, rax
        jz .overflow
        mov rdi, rax
        mov rsi, next
        call find_word
        test rax, rax
        jz .not_found
        jmp .sucsess
        
        
.sucsess:
        mov rdi, rax
        add rdi, 8
        push rdi
        call string_length
        pop rdi
        add rdi, rax
        inc rdi
        push rdi
        mov rdi, ok
        call print_string
        pop rdi
        call print_string
        call print_newline
        jmp .exit	
.overflow:
	mov rdi, overflow
	jmp .error_print
.not_found:
	mov rdi, error
	jmp .error_print
.error_print:
	call print_error
	jmp .exit
.exit:
	call exit


